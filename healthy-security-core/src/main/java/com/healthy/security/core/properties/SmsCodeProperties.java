package com.healthy.security.core.properties;

import lombok.Data;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * 短信验证码相关配置项
 */
@Data
public class SmsCodeProperties {

    /**
     * 验证码长度
     */
    private int length = 6;

    /**
     * 过期时间
     */
    private int expireIn = 60;

    /**
     * 要拦截的url集合
     */
    private Set<String> urls = new LinkedHashSet<>();
}